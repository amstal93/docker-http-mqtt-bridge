#
#   BUILD image
#
FROM node:8-alpine AS build

# Create the working dir
RUN mkdir -p /build

WORKDIR /build

# Install package dependencies
RUN apk add --update --no-cache python pkgconfig make g++ git

# Clone the source repo and install app.
RUN git clone https://github.com/petkov/http_to_mqtt.git . && \
    npm install

#
#   RUNTIME image
#
FROM node:8-alpine AS runtime

LABEL Author="MiGoller"

# Persist http_to_mqtt version information from /app/package.json
ARG ARG_APP_VERSION
ENV APP_VERSION=$ARG_APP_VERSION

# Set environment variables to default values
ENV AUTH_KEY=SetToSomethingVerySpecial

COPY --from=build /build /app

WORKDIR /app

# Install NodeJS dependencies
RUN npm install pm2 -g

# Copy PM2 config
COPY pm2app.yml .

EXPOSE 5000

CMD ["pm2-docker", "pm2app.yml"]
