#!/bin/sh

set -ev

# Install nodejs for version information, etc.
apk add --update --no-cache nodejs

# Get the corresponding package.json file for the app
wget -q https://raw.githubusercontent.com/petkov/http_to_mqtt/master/package.json -O ./package.json

# Get the app's version information
APP_VERSION=$(node -pe "require('./package.json')['version']")

# Build and push the Docker image with tag "latest"
docker build --build-arg ARG_APP_VERSION=$APP_VERSION -t "$CI_REGISTRY_IMAGE" .
docker push "$CI_REGISTRY_IMAGE"

# Split APP_VERSION for naming the tags
version=( ${APP_VERSION//./ } )

# Tag and push image for each additional tag
for tag in {"${version[0]}","${version[0]}.${version[1]}","${APP_VERSION}"}; do  
    docker tag "$CI_REGISTRY_IMAGE" "$CI_REGISTRY_IMAGE":${tag}
    docker push "$CI_REGISTRY_IMAGE":${tag}
done
